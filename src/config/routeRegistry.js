const bsat = (app) => {
  // Add your middleware here if needed
  app.use("/registraition", require("../app/bsat/registraition/registraition.router"));
  app.use("/studentRegistration", require("../app/bsat/studentRegistration/studentRegistration.router"));
  app.use("/uploadVideo", require("../app/bsat/uploadVideo/uploadVideo.router"));
  app.use("/uploadImage", require("../app/bsat/uploadImage/uploadImage.router"));
  app.use("/mockTestQuestions", require("../app/bsat/mockTestQuestions/mockTestQuestion.router"));
  app.use("/subject", require("../app/bsat/subject/subject.router"));
  app.use("/chapter", require("../app/bsat/chapter/chapter.router"));
  app.use("/addElerning", require("../app/bsat/addElerning/addElerning.router"));
  app.use("/feesForScholership", require("../app/bsat/feesForScholership/registrationForScholership.router"));
  app.use("/registrationForScholershipPayment", require("../app/bsat/registrationForScholershipPayment/registrationForScholershipPayment.router"));
  app.use("/submitmockTest", require("../app/bsat/submitmockTest/submitmockTest.router"));
  app.use("/purchaseStudyMaterial", require("../app/bsat/purchaseStudyMaterial/purchaseStudyMaterial.router"));
  app.use("/purchaseStudyMeterialPaymemnt", require("../app/bsat/purchaseStudyMeterialPaymemnt/purchaseStudyMeterialPaymemnt.router"));
  app.use("/studyMaterial", require("../app/bsat/studyMaterial/studyMaterial.router"));
  app.use("/mockTest", require("../app/bsat/mockTest/mockTest.router"));
  app.use("/bsatExamQuestion", require("../app/bsat/bsatExamQuestion/bsatExamQuestion.router"));
  app.use("/bsatExamTest", require("../app/bsat/bsatExamTest/bsatExamTest.router"));
  app.use("/submitBsatExamTest", require("../app/bsat/submitBsatExamTest/submitBsatExamTest.router"));
  app.use("/adminUser", require("../app/bsat/adminUser/adminUser.router"));
};

const routes = {
    bsat,
};

module.exports = routes[process.env.PROJ_NAME];
