
const mongoose = require('mongoose');

const chapterSchema = new mongoose.Schema({
  chaptername: { type: String, required: false },
  chapterImg: { type: String, required: false },
  subject: {
    type: mongoose.Schema.Types.ObjectId,
      ref: "subject",
      required: false,
  },
}, { timestamps: true });

module.exports = mongoose.model('chapter', chapterSchema);
