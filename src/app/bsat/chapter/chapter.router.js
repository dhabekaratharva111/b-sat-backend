
const router = require("express").Router();
const chapterController = require('./chapter.controller');

// Create Operation - Create chapter
router.post('/createChapter', chapterController.createChapter);

// Read Operation - Get all chapter
router.get('/getAllChapter', chapterController.getAllChapter);

// Read Operation - Get all chapter by Id 
router.get("/getAllChapterById/:id", chapterController.getAllChapterById);

// Read Operation - Get a single chapter by Id
router.get('/getChapterById/:id', chapterController.getChapterById);

router.get('/getChapterBySubject/:subject', chapterController.getChapterBySubject);

// Update Operation - Update chapter
router.put('/updateChapter/:id', chapterController.updateChapter);

// Delete Operation - Delete chapter
router.delete('/deleteChapter/:id', chapterController.deleteChapter);

module.exports = router;
