
const mongoose = require('mongoose');

const registrationForScholershipPaymentSchema = new mongoose.Schema({
  // Razorpay response data, if you want to store it for reference (optional).
  razorpayResData: { type: Object, required: false },
  // Payment status (e.g., 'paid', 'failed', 'pending').
  paymentStatus: { type: String, required: true },
  // Order status (e.g., 'Delivery', 'Cancelled', 'Pending' , 'Perparing).
  orderStatus: { type: String, required: true },
  // Total amount of the order.
  totalAmount: { type: Number, required: true },
  // User who placed the order.
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "studentRegistration",
    required: true,
  },
  bsatExamRegistrationId: { 
    type: mongoose.Schema.Types.ObjectId,
    ref: "registrationForScholership",
    required: true,
  },
  examFee: { type: Number, required: false },
  userImg: { type: String, required: false },
  userSignImg: { type: String, required: false },
  purchase: { type: String, default: false },
}, { timestamps: true });

module.exports = mongoose.model('registrationForScholershipPayment', registrationForScholershipPaymentSchema);
