
const router = require("express").Router();
const bsatExamQuestionController = require('./bsatExamQuestion.controller');

// Create Operation - Create bsatExamQuestion
router.post('/createBsatExamQuestion', bsatExamQuestionController.createBsatExamQuestion);

// Read Operation - Get all bsatExamQuestion
router.get('/getAllBsatExamQuestion', bsatExamQuestionController.getAllBsatExamQuestion);

// Read Operation - Get all bsatExamQuestion by Id 
router.get("/getAllBsatExamQuestionById/:id", bsatExamQuestionController.getAllBsatExamQuestionById);

// Read Operation - Get a single bsatExamQuestion by Id
router.get('/getBsatExamQuestionByBsatExamTestId/:id', bsatExamQuestionController.getBsatExamQuestionByBsatExamTestId);

// Update Operation - Update bsatExamQuestion
router.put('/updateBsatExamQuestion/:id', bsatExamQuestionController.updateBsatExamQuestion);

// Delete Operation - Delete bsatExamQuestion
router.delete('/deleteBsatExamQuestion/:id', bsatExamQuestionController.deleteBsatExamQuestion);

module.exports = router;
