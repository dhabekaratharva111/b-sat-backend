
const bsatExamQuestionModel = require('./bsatExamQuestion.model');
const bsatExamTestModel = require('./../bsatExamTest/bsatExamTest.model');

  // Create Operation - Create bsatExamQuestion
  const createBsatExamQuestion = (req, res) => {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }

    const options = req.body.optionList;
  
    const optionList = options.map((o) => ({
      optionValue: o.optionValue,
      optionId: generateUniqueId(),
      isCorrect: o.isCorrect,
    }));
  
    const bsatExamQuestion = new bsatExamQuestionModel({
      ...req.body,
      optionList,
    })
    ;
    bsatExamQuestion
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "bsatExamQuestion created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the bsatExamQuestion.",
        });
      });
  };

  // A simple function to generate a unique ID
  function generateUniqueId() {
    return Date.now().toString(36) + Math.random().toString(36).substr(2);
  }

  // Read Operation - Get all bsatExamQuestion
  const getAllBsatExamQuestion = (req, res) => {
    bsatExamQuestionModel.find()
    .populate({
      path: "bsatExamTestId",
    })
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "bsatExamQuestion fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "bsatExamQuestion not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the bsatExamQuestion.",
        });
      });
  };

  // Read Operation - Get all bsatExamQuestion by Id 
  const getAllBsatExamQuestionById  = (req, res) => {
    const id = req.params.id;
    const condition = { _id: id};

    bsatExamQuestionModel.find(condition)
    .populate({
      path: "bsatExamTestId",
    })
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "bsatExamQuestion fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "bsatExamQuestion not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the bsatExamQuestion.",
        });
      });
  };

  // Read Operation - Get a single bsatExamQuestion by Id
  const getBsatExamQuestionByBsatExamTestId = async(req, res) => {
    const bsatExamTestId = req.params.id;
    const condition = { bsatExamTestId: bsatExamTestId};
    const currentDate = new Date();
    
    let bsatExamQueDetail = await bsatExamTestModel.findById(bsatExamTestId).exec();
    const bsatExamDate = new Date(bsatExamQueDetail.bsatExamTestTimeDate);
    const calculatedExpiryDate =  new Date(bsatExamDate);
    calculatedExpiryDate.setMinutes(bsatExamDate.getMinutes() + parseInt(bsatExamQueDetail.bsatExamTestTimeDuration, 10));

    if (currentDate >= bsatExamDate && currentDate <= calculatedExpiryDate) {
     bsatExamQuestionModel.find(condition)
    .populate({
      path: "bsatExamTestId",
    })
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "bsatExamQuestion fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: {},
            dataCount: 0,
            message: 'bsatExamQuestion not found with ID=' + id,
            status: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the bsatExamQuestion.",
        });
      });
    } else if (currentDate > calculatedExpiryDate) {
      res.status(200).send({
        data: [],
        dataCount: 0,
        message: "Exam is expired..!",
        success: true,
        statusCode: 200,
      });
    } else {
      res.status(200).send({
        data: [],
        dataCount: 0,
        message: "Exam not started yet..!",
        success: true,
        statusCode: 200,
      });
    }    
  };

  // Update Operation - Update bsatExamQuestion
 const updateBsatExamQuestion = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;
  const options = req.body.optionList || [];

  const optionList = options.map((o) => ({
    optionValue: o.optionValue,
    optionId: generateUniqueId(),
    isCorrect: o.isCorrect || false,
  }));

  const updatedData = {
    ...req.body,
    optionList,
  };

  bsatExamQuestionModel.findByIdAndUpdate(id, updatedData, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "bsatExamQuestion was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message: 'Cannot update bsatExamQuestion with order ID=' + id + '. Maybe bsatExamQuestion was not found!',
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating bsatExamQuestion with ID=" + id,
      });
    });
};

// Delete Operation - Delete bsatExamQuestion
  const deleteBsatExamQuestion = (req, res) => {
    const id = req.params.id;
  
    bsatExamQuestionModel.findByIdAndDelete(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            message: "bsatExamQuestion was deleted successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(404).send({
            message: 'Cannot delete bsatExamQuestion with ID=' + id + '. Maybe bsatExamQuestion was not found!',
            success: false,
            statusCode: 404,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial.",
        });
      });
  };

module.exports = {
  createBsatExamQuestion,
  getAllBsatExamQuestion,
  getAllBsatExamQuestionById,
  getBsatExamQuestionByBsatExamTestId,
  updateBsatExamQuestion,
  deleteBsatExamQuestion
};
