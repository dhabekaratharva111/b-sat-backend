
const mongoose = require('mongoose');

const bsatExamQuestionSchema = new mongoose.Schema({
  bsatExamTestId: { 
    type: mongoose.Schema.Types.ObjectId,
      ref: "bsatExamTest",
      required: false,
   },
  question: { type: String, required: false },
  optionList: { type: Array, required: false }
}, { timestamps: true });

module.exports = mongoose.model('bsatExamQuestion', bsatExamQuestionSchema);
