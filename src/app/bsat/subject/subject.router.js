
const router = require("express").Router();
const subjectController = require('./subject.controller');

// Create Operation - Create subject
router.post('/createSubject', subjectController.createSubject);

// Read Operation - Get all subject
router.get('/getAllSubject', subjectController.getAllSubject);

// Read Operation - Get all subject by Id 
router.get("/getAllSubjectById/:id", subjectController.getAllSubjectById);

// Read Operation - Get a single subject by Id
router.get('/getSubjectById/:id', subjectController.getSubjectById);

router.get('/getSubjectByClass/:classId', subjectController.getSubjectByClass);

// Update Operation - Update subject
router.put('/updateSubject/:id', subjectController.updateSubject);

// Delete Operation - Delete subject
router.delete('/deleteSubject/:id', subjectController.deleteSubject);

module.exports = router;
