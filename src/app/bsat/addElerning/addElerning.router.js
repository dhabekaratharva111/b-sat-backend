
const router = require("express").Router();
const addElerningController = require('./addElerning.controller');

// Create Operation - Create addElerning
router.post('/createAddElerning', addElerningController.createAddElerning);

// Read Operation - Get all addElerning
router.get('/getAllAddElerning', addElerningController.getAllAddElerning);

// Read Operation - Get all addElerning by Id 
router.get("/getAllAddElerningById/:id", addElerningController.getAllAddElerningById);

// Read Operation - Get a single addElerning by Id
router.get('/getAddElerningById/:id', addElerningController.getAddElerningById);

router.get('/getYoutubeVideoByClassId/:classId/:medium', addElerningController.getYoutubeVideoByClassId);

// Update Operation - Update addElerning
router.put('/updateAddElerning/:id', addElerningController.updateAddElerning);

// Delete Operation - Delete addElerning
router.delete('/deleteAddElerning/:id', addElerningController.deleteAddElerning);

module.exports = router;
