
const mongoose = require('mongoose');

const addElerningSchema = new mongoose.Schema({
  classId: { type: String, required: false },
  medium: { type: String, required: false },
  videoName: { type: String, required: false },
  videoLink: { type: String, required: false },
}, { timestamps: true });

module.exports = mongoose.model('addElerning', addElerningSchema);
