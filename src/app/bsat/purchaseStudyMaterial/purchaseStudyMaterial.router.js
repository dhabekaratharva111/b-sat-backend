
const router = require("express").Router();
const purchaseStudyMaterialController = require('./purchaseStudyMaterial.controller');

// Create Operation - Create purchaseStudyMaterial
router.post('/createPurchaseStudyMaterial', purchaseStudyMaterialController.createPurchaseStudyMaterial);

// Read Operation - Get all purchaseStudyMaterial
router.get('/getAllPurchaseStudyMaterial', purchaseStudyMaterialController.getAllPurchaseStudyMaterial);

// Read Operation - Get all purchaseStudyMaterial by Id 
router.get("/getAllPurchaseStudyMaterialById/:id", purchaseStudyMaterialController.getAllPurchaseStudyMaterialById);

// Read Operation - Get a single purchaseStudyMaterial by Id
router.get('/getPurchaseStudyMaterialById/:id', purchaseStudyMaterialController.getPurchaseStudyMaterialById);

router.get('/getPurchaseStudyMaterialByClassId/:classId/:userId/:medium', purchaseStudyMaterialController.getPurchaseStudyMaterialByClassId);

// Update Operation - Update purchaseStudyMaterial
router.put('/updatePurchaseStudyMaterial/:id', purchaseStudyMaterialController.updatePurchaseStudyMaterial);

// Delete Operation - Delete purchaseStudyMaterial
router.delete('/deletePurchaseStudyMaterial/:id', purchaseStudyMaterialController.deletePurchaseStudyMaterial);

module.exports = router;
