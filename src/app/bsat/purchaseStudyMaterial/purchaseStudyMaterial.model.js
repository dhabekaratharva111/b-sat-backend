
const mongoose = require('mongoose');

const purchaseStudyMaterialSchema = new mongoose.Schema({
  classId: { type: String, required: false },
  medium: { type: String, required: false },
  materialFee: { type: Number, required: false }
}, { timestamps: true });

module.exports = mongoose.model('purchaseStudyMaterial', purchaseStudyMaterialSchema);
