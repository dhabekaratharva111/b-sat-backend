
const mongoose = require('mongoose');

const StudyMaterialSchema = new mongoose.Schema({
  classId: { type: String, required: false },
  medium: { type: String, required: false },
  studyMaterial: { type: String, required: false },
}, { timestamps: true });

module.exports = mongoose.model('StudyMaterial', StudyMaterialSchema);
