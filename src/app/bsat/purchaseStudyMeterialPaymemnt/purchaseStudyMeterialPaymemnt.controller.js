
const purchaseStudyMeterialPaymemntModel = require('./purchaseStudyMeterialPaymemnt.model');
const purchaseStudyMaterialModel = require('./../purchaseStudyMaterial/purchaseStudyMaterial.model');
const razorpayPaymentUtil = require("../../../shared/utils/razorpayPayment.util");

const proceedPayment = (req, res) => {
  razorpayPaymentUtil.proceedPayment(req, res);
};

  // Create Operation - Create purchaseStudyMeterialPaymemnt
  const createPurchaseStudyMeterialPaymemnt = (req, res) => {
    if (razorpayPaymentUtil.verifyPaymentSignature(req.body.razorpayResData)) {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
  
    const purchaseStudyMeterialPaymemnt = new purchaseStudyMeterialPaymemntModel(req.body);

    if (req.body.paymentStatus === "paid") {
      purchaseStudyMeterialPaymemnt.purchase = true;
    } else {
      purchaseStudyMeterialPaymemnt.purchase = false;
    }

    purchaseStudyMeterialPaymemnt
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "purchaseStudyMeterialPaymemnt created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the purchaseStudyMeterialPaymemnt.",
        });
      });
    } else {
      console.log("Signature verification failed.");
    }
  };

  // Read Operation - Get all purchaseStudyMeterialPaymemnt
  const getAllPurchaseStudyMeterialPaymemnt = (req, res) => {
    purchaseStudyMeterialPaymemntModel.find()
    .populate({
      path: "userId",
    })
    .populate({
      path: "materialId",
    })
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "purchaseStudyMeterialPaymemnt fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "purchaseStudyMeterialPaymemnt not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the purchaseStudyMeterialPaymemnt.",
        });
      });
  };

  // Read Operation - Get all purchaseStudyMeterialPaymemnt by Id 
  const getAllPurchaseStudyMeterialPaymemntById  = (req, res) => {
    const id = req.params.id;
    const condition = { _id: id};

    purchaseStudyMeterialPaymemntModel.find(condition)
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "purchaseStudyMeterialPaymemnt fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "purchaseStudyMeterialPaymemnt not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the purchaseStudyMeterialPaymemnt.",
        });
      });
  };

  // Read Operation - Get a single purchaseStudyMeterialPaymemnt by Id
  const getPurchaseStudyMeterialPaymemntByUserId = async (req, res) => {
    try {
        const userId = req.body.userId;
        const classId = req.body.classId;

        const paymentData = await purchaseStudyMeterialPaymemntModel.findOne({ userId, 'payFee.classId': classId });

        if (paymentData) {
            if (paymentData.purchase === 'true') {
                const studyMaterialData = await purchaseStudyMaterialModel.findOne({ classId });

                if (studyMaterialData) {
                    return res.status(200).send({
                        paymentData: paymentData,
                        studyMaterialData: studyMaterialData,
                        message: "Purchase study material and payment fetched successfully!",
                        success: true,
                        statusCode: 200,
                    });
                } else {
                    return res.status(404).send({
                        paymentData: paymentData,
                        studyMaterialData: null,
                        message: `No study material found for classId=${classId}.`,
                        success: false,
                        statusCode: 404,
                    });
                }
            } else {
                return res.status(200).send({
                    paymentData: paymentData,
                    studyMaterialData: null,
                    message: `Purchase study material not found or payment not yet made for userId=${userId} and classId=${classId}.`,
                    success: false,
                    statusCode: 200,
                });
            }
        } else {
            return res.status(404).send({
                paymentData: null,
                studyMaterialData: null,
                message: `No purchase study material payment found for userId=${userId} and classId=${classId}.`,
                success: false,
                statusCode: 404,
            });
        }
    } catch (error) {
        return res.status(500).send({
            message: error.message || "Some error occurred while processing the request.",
            success: false,
            statusCode: 500,
        });
    }
  };

  // Update Operation - Update purchaseStudyMeterialPaymemnt
  const updatePurchaseStudyMeterialPaymemnt = (req, res) => {
    if (!req.body) {
      return res.status(400).send({
        message: "Data to update can not be empty!",
      });
    }

    const id = req.params.id;

    purchaseStudyMeterialPaymemntModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
      .then((data) => {
        if (data) {
          res.status(200).send({
            message: "purchaseStudyMeterialPaymemnt was updated successfully.",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(404).send({
            message: 'Cannot update purchaseStudyMeterialPaymemnt with order ID=' + id + '. Maybe purchaseStudyMeterialPaymemnt was not found!',
            success: false,
            statusCode: 404,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: "Error updating purchaseStudyMeterialPaymemnt with ID=" + id,
        });
      });
  };

  // Read Operation - Get a single purchaseStudyMaterial by Id
  const getPurchaseStudyMaterialByClassId = (req, res) => {
    const classId = req.params.classId;
  
    purchaseStudyMaterialModel.findOne({ classId: classId})
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: 1,
            message: "purchaseStudyMaterial fetched successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: null,
            dataCount: 0,
            message: 'purchaseStudyMaterial not found with classId=' + classId,
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occurred while retrieving the purchaseStudyMaterial.",
          success: false,
          statusCode: 500,
        });
      });
  };

// Delete Operation - Delete purchaseStudyMeterialPaymemnt
  const deletePurchaseStudyMeterialPaymemnt = (req, res) => {
    const id = req.params.id;
  
    purchaseStudyMeterialPaymemntModel.findByIdAndDelete(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            message: "purchaseStudyMeterialPaymemnt was deleted successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(404).send({
            message: 'Cannot delete purchaseStudyMeterialPaymemnt with ID=' + id + '. Maybe purchaseStudyMeterialPaymemnt was not found!',
            success: false,
            statusCode: 404,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial.",
        });
      });
  };

module.exports = {
  createPurchaseStudyMeterialPaymemnt,
  getAllPurchaseStudyMeterialPaymemnt,
  getAllPurchaseStudyMeterialPaymemntById,
  getPurchaseStudyMeterialPaymemntByUserId,
  updatePurchaseStudyMeterialPaymemnt,
  deletePurchaseStudyMeterialPaymemnt,
  proceedPayment,
  getPurchaseStudyMaterialByClassId
};
