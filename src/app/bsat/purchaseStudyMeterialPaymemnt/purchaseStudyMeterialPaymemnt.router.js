
const router = require("express").Router();
const purchaseStudyMeterialPaymemntController = require('./purchaseStudyMeterialPaymemnt.controller');
const purchaseStudyMaterialController = require('./../purchaseStudyMaterial/purchaseStudyMaterial.controller');

// Create Operation - Create purchaseStudyMeterialPaymemnt
router.post('/createPurchaseStudyMeterialPaymemnt', purchaseStudyMeterialPaymemntController.createPurchaseStudyMeterialPaymemnt);

// Read Operation - Get all purchaseStudyMeterialPaymemnt
router.get('/getAllPurchaseStudyMeterialPaymemnt', purchaseStudyMeterialPaymemntController.getAllPurchaseStudyMeterialPaymemnt);
router.post('/proceedPayment', purchaseStudyMeterialPaymemntController.proceedPayment);

// Read Operation - Get all purchaseStudyMeterialPaymemnt by Id 
router.get("/getAllPurchaseStudyMeterialPaymemntById/:id", purchaseStudyMeterialPaymemntController.getAllPurchaseStudyMeterialPaymemntById);

// Read Operation - Get a single purchaseStudyMeterialPaymemnt by Id
router.post("/getPurchaseStudyMeterialPaymemntByUserId", purchaseStudyMeterialPaymemntController.getPurchaseStudyMeterialPaymemntByUserId);

router.get('/getPurchaseStudyMaterialByClassId/:classId', purchaseStudyMaterialController.getPurchaseStudyMaterialByClassId);

// Update Operation - Update purchaseStudyMeterialPaymemnt
router.put('/updatePurchaseStudyMeterialPaymemnt/:id', purchaseStudyMeterialPaymemntController.updatePurchaseStudyMeterialPaymemnt);

// Delete Operation - Delete purchaseStudyMeterialPaymemnt
router.delete('/deletePurchaseStudyMeterialPaymemnt/:id', purchaseStudyMeterialPaymemntController.deletePurchaseStudyMeterialPaymemnt);

module.exports = router;
