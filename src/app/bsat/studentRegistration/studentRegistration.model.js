const mongoose = require('mongoose');

const studentRegistrationSchema = new mongoose.Schema({
  firstName: { type: String, required: false },
  middleName: { type: String, required: false },
  lastname: { type: String, required: false },
  email: { type: String, required: false },
  address1: { type: String, required: false },
  address2: { type: String, required: false },
  state: { type: String, required: false },
  pincode: { type: String, required: false },
  DOB: { type: String, required: false },
  gender: { type: String, required: false },
  mobileNo: { type: Number, required: false },
  userId: {  type: String, required: false },
  password: { type: String, required: false },
  confirmpassword: { type: String, required: false },
  class: { type: String, required: false },
  medium: { type: String, required: false },
  otp: { type: String, required: false },
  role: { type: String, required: false },
  checkBox: { type: String, required: false },
  schoolName: { type: String, required: false },
  userType: { type: String, required: false },
  coordinatorCode: { type: String, required: false },
  coordinatorName :{ type: String, required: false },

}, { timestamps: true });

module.exports = mongoose.model('studentRegistration', studentRegistrationSchema);
