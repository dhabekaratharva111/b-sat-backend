
const mongoose = require('mongoose');

const registraitionSchema = new mongoose.Schema({
  email: { type: String, required: false },
  name: { type: String, required: false },
  mobileNo: { type: String, required: false },
  DOB: { type: String, required: false },
  twelfthPass: { type: String, required: false },
  termsAndConditions: { type: String, required: false },
  age: { type: String, required: false },
  coordinatorCode: { type: String, required: false },
}, { timestamps: true });

module.exports = mongoose.model('registraition', registraitionSchema);
