
const router = require("express").Router();
const registraitionController = require('./registraition.controller');

// Create Operation - Create registraition
router.post('/createRegistraition', registraitionController.createRegistraition);

// Read Operation - Get all registraition
router.post('/getAllRegistraition', registraitionController.getAllRegistraition);

// Read Operation - Get all registraition by Id 
router.get("/getAllRegistraitionById/:id", registraitionController.getAllRegistraitionById);

// Read Operation - Get a single registraition by Id
router.get('/getRegistraitionById/:id', registraitionController.getRegistraitionById);

// Update Operation - Update registraition
router.put('/updateRegistraition/:id', registraitionController.updateRegistraition);

// Delete Operation - Delete registraition
router.delete('/deleteRegistraition/:id', registraitionController.deleteRegistraition);

module.exports = router;
