const submitBsatExamTestModel = require("./submitBsatExamTest.model");
const bsatExamQuestionModel = require("./../bsatExamQuestion/bsatExamQuestion.model");

// Create Operation - Create submitBsatExamTest
const createSubmitBsatExamTest = async (req, res) => {
  try {
    if (!req.body) {
      return res.status(400).json({ message: "Content can not be empty!" });
    }

    req.body.totalQuestion = await bsatExamQuestionModel
      .countDocuments({ bsatExamTestId: req.body.bsatExamTestId })
      .exec();
    req.body.attemptedQuestion = 0;
    req.body.unAttemptedQuestion = 0;
    req.body.correctAnswer = 0;
    req.body.wrongAnswer = 0;
    req.body.negativeMarks = 0;
    req.body.correctQuestionNumbers = [];
    req.body.wrongQuestionNumbers = [];

    for (let i = 0; i < req.body.answerSheet.length; i++) {
      const element = req.body.answerSheet[i];
      req.body.attemptedQuestion = req.body.attemptedQuestion + 1;
      try {
        let bsatExamQueDetail = await bsatExamQuestionModel
          .findById(element.questionId)
          .select("optionList")
          .exec();
        const matchingOption = bsatExamQueDetail?.optionList.find(
          (option) => option.optionId === element.answerId
        );
        element["isCorrect"] = matchingOption?.isCorrect ?? false;
        if (element.isCorrect) {
          req.body.correctAnswer = req.body.correctAnswer + 1;
          req.body.correctQuestionNumbers.push(i + 1);
        } else {
          req.body.wrongAnswer = req.body.wrongAnswer + 1;
          req.body.wrongQuestionNumbers.push(i + 1);
        }
      } catch (error) {
        element["isCorrect"] = false;
        req.body.wrongQuestionNumbers.push(i + 1);
      }
    }

    req.body.unAttemptedQuestion =
      req.body.totalQuestion - req.body.attemptedQuestion;
    req.body.totalMarks = req.body.totalQuestion * 2;
    req.body.negativeMarks = req.body.wrongAnswer * 0.5;

    // Calculate the marks with 2 marks per correct answer
    // 0.5 marks deduction for each wrong answer
    req.body.obtainedMarks =
      req.body.correctAnswer * 2 - req.body.wrongAnswer * 0.5;

    // Calculate the percentage
    const percentageGot = Number(
      ((req.body.obtainedMarks / req.body.totalMarks) * 100).toFixed(2)
    );
    // Check if the percentage is greater than or equal to 40
    const passingCriteria = 40;

    req.body.testResult = percentageGot >= passingCriteria ? "Pass" : "Fail";

    req.body.obtainedPercentage = percentageGot;

    // Write logic here to save in DB and send the responise back to user
    const submitBsatExamTest = new submitBsatExamTestModel(req.body);
    submitBsatExamTest
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "submitBsatExamTest created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message ||
            "Some error occurred while creating the submitBsatExamTest.",
        });
      });
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

// Read Operation - Get all submitBsatExamTest
const getAllSubmitBsatExamTest = (req, res) => {
  submitBsatExamTestModel
    .find()
    .populate({
      path: "userId",
    })
    .populate({
      path: "bsatExamTestId",
    })
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "submitBsatExamTest fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "submitBsatExamTest not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the submitBsatExamTest.",
      });
    });
};

// Read Operation - Get all submitBsatExamTest by Id
const getAllSubmitBsatExamTestById = (req, res) => {
  const id = req.params.id;
  const condition = { _id: id };

  submitBsatExamTestModel
    .find(condition)
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "submitBsatExamTest fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "submitBsatExamTest not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the submitBsatExamTest.",
      });
    });
};

// Read Operation - Get a single submitBsatExamTest by Id
const getSubmitBsatExamTestById = (req, res) => {
  const id = req.params.id;
  submitBsatExamTestModel
    .findById(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "submitBsatExamTest fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "submitBsatExamTest not found with ID=" + id,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the submitBsatExamTest.",
      });
    });
};

// Update Operation - Update submitBsatExamTest
const updateSubmitBsatExamTest = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  submitBsatExamTestModel
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "submitBsatExamTest was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot update submitBsatExamTest with order ID=" +
            id +
            ". Maybe submitBsatExamTest was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating submitBsatExamTest with ID=" + id,
      });
    });
};

// Delete Operation - Delete submitBsatExamTest
const deleteSubmitBsatExamTest = (req, res) => {
  const id = req.params.id;

  submitBsatExamTestModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "submitBsatExamTest was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete submitBsatExamTest with ID=" +
            id +
            ". Maybe submitBsatExamTest was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

module.exports = {
  createSubmitBsatExamTest,
  getAllSubmitBsatExamTest,
  getAllSubmitBsatExamTestById,
  getSubmitBsatExamTestById,
  updateSubmitBsatExamTest,
  deleteSubmitBsatExamTest,
};
