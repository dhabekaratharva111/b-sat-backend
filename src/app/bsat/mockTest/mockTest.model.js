
const mongoose = require('mongoose');

const mockTestSchema = new mongoose.Schema({
  classId: { type: String, required: false },
  medium: { type: String, required: false },
  testName: { type: String, required: false },
  testTimeDuration: { type: String, required: false },
}, { timestamps: true });

module.exports = mongoose.model('mockTest', mockTestSchema);
