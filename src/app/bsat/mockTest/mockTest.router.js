
const router = require("express").Router();
const mockTestController = require('./mockTest.controller');

// Create Operation - Create mockTest
router.post('/createMockTest', mockTestController.createMockTest);

// Read Operation - Get all mockTest
router.get('/getAllMockTest', mockTestController.getAllMockTest);

// Read Operation - Get all mockTest by Id 
router.get("/getAllMockTestByIds/:classId/:medium", mockTestController.getAllMockTestByIds);

// Read Operation - Get a single mockTest by Id
router.get('/getMockTestById/:id', mockTestController.getMockTestById);

// router.get('/getMockTestByClassId/:classId', mockTestController.getMockTestByClassId);
router.get('/getMockTestByClassId/:classId/:userId/:medium', mockTestController.getMockTestByClassId);

// Update Operation - Update mockTest
router.put('/updateMockTest/:id', mockTestController.updateMockTest);

// Delete Operation - Delete mockTest
router.delete('/deleteMockTest/:id', mockTestController.deleteMockTest);

module.exports = router;
