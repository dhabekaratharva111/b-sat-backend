
const router = require("express").Router();
const bsatExamTestController = require('./bsatExamTest.controller');

// Create Operation - Create bsatExamTest
router.post('/createBsatExamTest', bsatExamTestController.createBsatExamTest);

// Read Operation - Get all bsatExamTest
router.get('/getAllBsatExamTest', bsatExamTestController.getAllBsatExamTest);

// Read Operation - Get all bsatExamTest by Id 
router.get("/getAllBsatExamTestById/:id", bsatExamTestController.getAllBsatExamTestById);

// Read Operation - Get a single bsatExamTest by Id
router.get('/getBsatExamTestByClassId/:classId/:userId', bsatExamTestController.getBsatExamTestByClassId);

// Update Operation - Update bsatExamTest
router.put('/updateBsatExamTest/:id', bsatExamTestController.updateBsatExamTest);

// Delete Operation - Delete bsatExamTest
router.delete('/deleteBsatExamTest/:id', bsatExamTestController.deleteBsatExamTest);

module.exports = router;
