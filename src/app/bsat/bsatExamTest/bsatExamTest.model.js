
const { Timestamp } = require('mongodb');
const mongoose = require('mongoose');

const bsatExamTestSchema = new mongoose.Schema({
  classId: { type: String, required: false },
  bsatExamTestName: { type: String, required: false },
  bsatExamTestTimeDuration: { type: String, required: false },
  bsatExamTestTimeDate: { type: String, required: false }
}, { timestamps: true });

module.exports = mongoose.model('bsatExamTest', bsatExamTestSchema);
