const jwt = require("jsonwebtoken");

// Secret key to sign the token
const secretKey = process.env.JWT_SECRET_KEY;

// Middleware function for JWT authentication
const authenticateToken = (req, res, next) => {
  // Get the authorization header from the request
  const authHeader = req.headers["authorization"];
  // Extract the token from the header
  const token = authHeader && authHeader.split(" ")[1];

  // If there's no token, return an error response
  if (!token) {
    return res.status(401).json({ message: "No token provided" });
  }

  // Verify the token with the secret key
  jwt.verify(token, secretKey, (err, decoded) => {
    if (err) {
      return res.status(403).json({ message: "Token is not valid" });
    }
    // If the token is valid, attach the decoded data to the request object
    req.user = decoded;
    next();
  });
};

module.exports = authenticateToken;
